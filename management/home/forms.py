from django import forms
from .models import Proyecto

class ProjectModelForm(forms.ModelForm):
    class Meta:
        model = Proyecto
        fields = [
            "nombre",
            "team",
            "fecha_creacion",
            "fecha_entrega",
            "descripcion",
            "id_status"
        ]
