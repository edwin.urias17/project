from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.forms.utils import ErrorList
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView, CreateView, UpdateView #[ListView, CreateView, DeleteView, DetailView, UpdateView]
from django.views import generic
from .forms import ProjectModelForm
from .models import Proyecto, User
from .models import Status

# Create your views here.

class indexView(ListView):
    queryset = Proyecto.objects.all()
    template_name = "home/index.html"

    def get_context_data(self, *args, **kwargs):
        context = super(indexView, self).get_context_data(*args, **kwargs)
        return context

class createProject(CreateView):
    form_class = ProjectModelForm
    template_name = "project/new.html"
    success_url = "/"

    def get_context_data(self, *args, **kwargs):
        kwargs['object_list'] = User.objects.order_by('id')
        context = super(createProject, self).get_context_data(*args, **kwargs)
        return context

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(createProject, self).form_valid(form)
    # if request.method == "POST":
    #     form = ProjectModelForm(request.POST)
    #
    #     if form.is_valid():
    #         return super(createProject).form_valid(form)
    # else:
    #     form = ProjectModelForm()
    #
    # return render(request, '/', {'form': form})
