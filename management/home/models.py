from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

# Create your models here.

class Proyecto(models.Model):
    nombre = models.CharField(max_length=256)
    id_lider = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    team = models.ForeignKey('Team', on_delete=models.PROTECT, null=True)
    fecha_creacion = models.DateField(default=None)
    fecha_entrega = models.DateField(default=None, blank=True, null=True)
    descripcion = models.CharField(max_length=256)
    id_status = models.ForeignKey('Status', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.nombre)

class Team(models.Model):
    team = models.ManyToManyField(User)

    def __str__(self):
        return str(self.team)

class Status(models.Model):
    status = models.CharField(max_length=256)

    def __str__(self):
        return str(self.status)

class ComentarioProyecto(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    id_proyecto = models.ForeignKey('Proyecto', on_delete=models.CASCADE, null=True)
    fecha = models.DateField(default=None)
    comentario = models.CharField(max_length=256)

    def __str__(self):
        return str(self.user)

class ActividadProyecto(models.Model):
    user = models.ManyToManyField(User)
    id_proyecto = models.ForeignKey('Proyecto', on_delete=models.CASCADE)
    fecha_entrega = models.DateField(default=None)

    def __str__(self):
        return str(self.user)
